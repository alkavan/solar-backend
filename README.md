# Solar Backend
This application provides a multi-threaded import option from
'NASA Mars Rover API' of manifests and rover photos.
As well a JSON based webservice to expose imported data,
and a small frontal application to show graphs about
the data using `d3` and `c3` JavaScript libraries.

##Installation
This package is using `worker_threads`, and was tested on latest Node v11.
If you have Node v10 use `--experimental-worker` when running `node`.


### Install packages
```
npm install
```

### Start MongoDB with Docker
```
docker-compose up
```

### Import rover photos and manifest 
```
npm run import
```
Or without using npm:
```
node --experimental-worker src/import.js
```
This would start the import process,
and open a thread for each rover manifest.

It successfully worked for importing data of 100 days,
producing 52581 photos in database.
Number of days can be modified by 
adjusting `MAX_SOL_IMPORT` in `src/workers/photo_import.js` 

### Start the API web service
```
npm run serve-api
```
Check [http://localhost:3000/](http://localhost:3000/).
You should see a list of links to application functions in JSON format.

### Start the web application
```
npm run serve-app
```
Check [http://localhost:8080/](http://localhost:8080/).
If data was imported you should see 3 graphs about photo data.

## Notes
It should be possible to make the process even more multi-threaded by
converting `importPhotoCollection()` to use worker in `src/import.js`,
but I decided not to do this right now cause it requiring monitoring
the workers and managing a pool (too much would open otherwise).
