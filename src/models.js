const mongoose = require('mongoose');

/**
 * RoverManifest Database Model
 * @type {Model}
 */
const RoverManifest = mongoose.model('RoverManifest', {
    name: {
        type: String,
        required: true,
        unique: true
    },
    landing_date: Date,
    launch_date: Date,
    status: String,
    max_sol: Number,
    max_date: Date,
    total_photos: Number,
    photos: Array
}, 'rover_manifests');

/**
 * RoverPhoto Database Model
 * @type {Model}
 */
const RoverPhoto = mongoose.model('RoverPhoto', {
    id: {
        type: Number,
        required: true,
        unique: true
    },
    sol: {
        type: Number,
        required: true
    },
    camera: Object,
    img_src: {
        type: String,
        required: true
    },
    earth_date: Date,
    rover: Object
}, 'rover_photos');

// Export models
module.exports = {
    RoverManifest: RoverManifest,
    RoverPhoto: RoverPhoto,
};
