'use strict';

const NASA_API_BASE = 'https://api.nasa.gov/mars-photos/api/v1';

const ACTION_MANIFESTS = 'manifests';
const ACTION_ROVERS = 'rovers';
const ACTION_ROVER_PHOTOS = 'photos';

/**
 * Class MarsPhoto
 */
class MarsPhoto {
    /**
     * Default constructor
     * @param {string} nasaApiKey
     */
    constructor(nasaApiKey) {
        this.apiBaseUrl = NASA_API_BASE;
        this.apiKey = nasaApiKey;
    }

    /**
     *
     * @param {string} roverName
     * @returns {string}
     */
    buildManifestsUrl(roverName) {
        const url = new URL(`${this.apiBaseUrl}/${ACTION_MANIFESTS}/${roverName}`);
        url.searchParams.set('api_key', this.apiKey);
        return url.toString();
    }

    /**
     *
     * @param {number} roverName
     * @returns {string}
     */
    buildRoversUrl(roverName) {
        const url = new URL(`${this.apiBaseUrl}/${ACTION_ROVERS}/${roverName}`);
        url.searchParams.set('api_key', this.apiKey);
        return url.toString();
    }

    /**
     *
     * @param {string} roverName
     * @param {number} sol
     * @returns {string}
     */
    buildRoverPhotosUrl(roverName, sol) {
        const url = new URL(`${this.apiBaseUrl}/${ACTION_ROVERS}/${roverName}/${ACTION_ROVER_PHOTOS}`);
        url.searchParams.set('api_key', this.apiKey);
        url.searchParams.set('sol', sol.toString());
        return url.toString();
    }
}

module.exports = MarsPhoto;
