const chalk = require('chalk');
const request = require('request');
const mongoose = require('mongoose');
const datastore = mongoose.connect('mongodb://localhost:27017/nasa', { useNewUrlParser: true });

// Multi threading
const { Observable, timer } = require('rxjs');
const { takeWhile, takeUntil } = require('rxjs/operators');
const { Worker } = require('worker_threads');
const COMPLETE_SIGNAL = 'COMPLETE';
const WORKER_TIMEOUT = 300;

const TAG_INFO = chalk.blue('[INFO]: ');
const TAG_WARNING = chalk.blue('[WARN]: ');
const TAG_ERROR = chalk.red('[ERROR]: ');
const TAG_DONE = chalk.green('[DONE]: ');

// const NASA_API_KEY = 'DEMO_KEY';
const NASA_API_KEY = 'PqzUl7AhVzgnGio9lTFWbBVI5VXUJyii5AnvY1XH';

const C = require('./constants');

const models = require('./models');
const RoverManifest = models.RoverManifest;
const RoverPhoto = models.RoverPhoto;

const MarsPhoto = require('./domain/MarsPhoto');

// Start import
console.log(chalk.blueBright("Importing data from Mars Rover Photos API ..."));

// For all rovers, each rover in different thread
for(const roverKey in C.ROVER) {
    console.log(TAG_INFO
        + `Importing rover '${C.ROVER[roverKey]}' data ...`);

    importRoverManifest(C.ROVER[roverKey]).then((result) => {
        console.log(TAG_DONE
            + `Imported '${result.roverName}' rover manifest, period of ${result.solCount} sols, `
            + `and total of ${result.photoCount} photos.`);
    }).catch(function (err) {
        console.log(TAG_ERROR + err);
    });
}

/**
 * Run worker task with given data
 * @param data
 * @param onCompleted
 * @returns {Observable<any>}
 */
function runWorkerTask(data, onCompleted) {
    return new Observable(observer => {
        const workerData = JSON.stringify(data);
        const worker = new Worker('./src/workers/photo_import.js', { workerData });

        worker.on('message', message =>  observer.next(message));
        worker.on('error', error => observer.error(error));
        worker.on('exit', code => {
            if (code !== 0) {
                observer.error(TAG_ERROR + `Worker stopped with exit code ${code}`);
            } else {
                onCompleted();
                observer.next(COMPLETE_SIGNAL);
                observer.complete();
            }
        });
    });
}

/**
 * Import rover manifest by rover key
 *
 * @param roverKey
 */
function importRoverManifest(roverKey) {

    return new Promise(function(resolve, reject) {
        const photoApi = new MarsPhoto(NASA_API_KEY);
        const url = photoApi.buildManifestsUrl(roverKey);

        console.log(TAG_INFO + url);

        request(url, {json: true}, (err, apiRes, body) => {
            if (err) {
                reject(err);
                return;
            }

            const manifest = new RoverManifest(body["photo_manifest"]);

            manifest.save()
                .then((record) => {
                    let solCount = 0;

                    const worker$ = runWorkerTask(record, () => {
                        console.log(TAG_INFO + `Photo import complete for '${record["name"]}' rover.`);
                    });

                    // receive messages from worker until it completes but only wait for MAX_WAIT_TIME
                    worker$.pipe(
                        takeWhile(message => message !== COMPLETE_SIGNAL),
                        takeUntil(timer(WORKER_TIMEOUT * 1000))
                    ).subscribe(
                        result => {
                            const photoCollection = JSON.parse(result);
                            console.log(TAG_INFO + `Saving ${photoCollection.length} photos to database.`);
                            importPhotoCollection(photoCollection);
                            solCount++;
                        },
                        error => {
                            console.error(TAG_ERROR + `Worker error: ${error}`);
                            reject(error);
                        },
                        () => {
                            resolve({
                                roverName: record["name"],
                                photoCount: record["total_photos"],
                                solCount: solCount,
                            });
                        }
                    );
                })
                .catch((err) => {
                    reject(err);
                });
        });
    });
}

/**
 * Import a collection of photos
 *
 * @param {Array} photos
 */
function importPhotoCollection(photos) {
    for(const photo of photos)
    {
        new RoverPhoto(photo).save()
            .catch((err) => {
                console.log(err);
            });
    }
}
