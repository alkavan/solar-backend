const ROVER = {
    CURIOSITY:   'curiosity',
    OPPORTUNITY: 'opportunity',
    SPIRIT:      'spirit',
};

const CAMERAS = {
    FHAZ:    'FHAZ',
    RHAZ:    'RHAZ',
    MAST:    'MAST',
    CHEMCAM: 'CHEMCAM',
    MAHLI:   'MAHLI',
    MARDI:   'MARDI',
    NAVCAM:  'NAVCAM',
    PANCAM:  'PANCAM',
    MINITES: 'MINITES'
};

module.exports = {
    ROVER: ROVER,
    CAMERAS: CAMERAS
};
