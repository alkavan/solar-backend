const request = require('request');
const chalk = require('chalk');

const MarsPhoto = require('../domain/MarsPhoto');

// run with node --experimental-worker index.js on Node.js 10.x
const { workerData, parentPort } = require('worker_threads');

// const NASA_API_KEY = 'DEMO_KEY';
const NASA_API_KEY = 'PqzUl7AhVzgnGio9lTFWbBVI5VXUJyii5AnvY1XH';

const logTag = `WORKER#${process.pid}`;

const record = JSON.parse(workerData);
const solCount = record["photos"].length;

const MAX_SOL_IMPORT = 100;

const TAG_WORKER_INFO = chalk.yellow(`[INFO:${logTag}]: `);

console.log(TAG_WORKER_INFO + `Starting import of ${solCount} sols.`);

/**
 * Do worker workload
 *
 * @param sol
 * @returns {Promise<any>}
 */
const doWork = (sol) => {
    return new Promise((resolve, reject) => {
        const photoApi = new MarsPhoto(NASA_API_KEY);
        const url = photoApi.buildRoverPhotosUrl(record["name"].toLowerCase(), sol);

        console.log(TAG_WORKER_INFO + `Fetching -> ${url}`);
        console.log(TAG_WORKER_INFO
            + `Importing ${record["total_photos"]} photos from manifest of ${record["name"]} rover.`);

        // Make the request
        request(url, {json: true}, (err, apiRes, body) => {
            if (err) {
                reject(err);
            }

            console.log(TAG_WORKER_INFO + `Got ${body["photos"].length} photos for sol ${sol}`);
            resolve(body);
        });
    })
};

/**
 * Does import operations and reports results to main thread
 *
 * @returns {Promise<void>}
 */
async function activate() {
    for(const solRecord of record["photos"].slice(0, MAX_SOL_IMPORT)) {
        console.log(TAG_WORKER_INFO
            + `Importing sol [${solRecord["sol"]}/${solCount}] -> ` + JSON.stringify(solRecord));
        let result = await doWork(solRecord["sol"]);

        parentPort.postMessage(JSON.stringify(result["photos"]));
    }
}

// Do worker import
activate().finally(() => {
    console.log(TAG_WORKER_INFO + 'Worker done processing!');
});
