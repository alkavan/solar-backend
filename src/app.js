import c3 from 'c3';
import d3 from 'd3';

const byRoverChart = c3.generate({
    bindto: '#byRoverChart',
    data: {
        url: 'http://localhost:3000/data/photosByRover',
        mimeType: 'json'
    },
});

const bySolChart = c3.generate({
    bindto: '#bySolChart',
    data: {
        url: 'http://localhost:3000/data/photosBySol',
        mimeType: 'json'
    },
});

const byDateChart = c3.generate({
    bindto: '#byDateChart',
    data: {
        x: 'Date',
        url: 'http://localhost:3000/data/photosByDate',
        mimeType: 'json'
    },
    axis: {
        x: {
            type: 'timeseries',
            tick: {
                format: '%Y-%m-%d'
            }
        }
    },
    zoom: {
        enabled: true
    }
});

