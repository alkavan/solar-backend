const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const mongoose = require('mongoose');
const datastore = mongoose.connect('mongodb://localhost:27017/nasa', { useNewUrlParser: true });

const models = require('./models');
const RoverManifest = models.RoverManifest;
const RoverPhoto = models.RoverPhoto;

const HTTP_PORT = 3000;

const app = express();

app.use(bodyParser.json());
app.use(cors({
    origin: 'http://localhost:8080',
}));

app.use(function (req, res, next) {
    res.contentType('application/json');
    next();
});

app.get('/', (req, res) => {
    res.send({
        rover: {
            manifest: 'http://localhost:3000/manifest',
            photos: 'http://localhost:3000/photos',
            photos_example_1: 'http://localhost:3000/photos?cameras[]=NAVCAM&cameras[]=FHAZ&cameras[]=RHAZ',
            photos_example_2: 'http://localhost:3000/photos?rover_id=7',
            photos_example_3: 'http://localhost:3000/photos?rover=Spirit',
            photos_example_4: 'http://localhost:3000/photos?sol=1',
            photos_chart_rover: 'http://localhost:3000/data/photosByRover',
            photos_chart_sol: 'http://localhost:3000/data/photosBySol',
            photos_chart_date: 'http://localhost:3000/data/photosByDate',
        },
    });
});

app.get('/manifest', (req, res) => {
    RoverManifest.find({}, function (err, docs) {
        if (err) {
            res.send({error: err});
            return;
        }

        res.send(docs);
    });
});

/**
 * Build a query for photo find from request parameters
 * @param req
 */
function buildPhotosQuery(req) {
    const query = {};

    if(req.query["cameras"]) {
        query["camera.name"] = {"$in" : req.query["cameras"]};
    }

    if(req.query["rover_id"]) {
        query["rover.id"] = parseInt(req.query["rover_id"]);
    }

    if(req.query["rover"]) {
        query["rover.name"] = req.query["rover"];
    }

    if(req.query["sol"]) {
        query["sol"] = req.query["sol"];
    }

    return query;

}

/**
 * Photo Index
 */
app.get('/photos', (req, res) => {
    const query = buildPhotosQuery(req);

    RoverPhoto.find(query, function (err, docs) {
        if (err) {
            res.send({error: err});
            return;
        }

        res.send(docs);
    });
});

app.get('/data/photosByRover', (req, res) => {
    RoverPhoto.aggregate(
        [
            {
                $group : {
                    _id: "$rover.name",
                    count: { $sum: 1 }
                }
            }
        ]
    ).exec().then((results) => {
        const x1 = results.map(function (item) {
            return item["_id"];
        });

        const y1 = results.map(function (item) {
            return item["count"];
        });

        res.send({
            Rover: x1,
            Photos: y1
        });
    });
});

app.get('/data/photosBySol', (req, res) => {
    RoverPhoto.aggregate(
        [
            {
                $group : {
                    _id: "$sol",
                    count: { $sum: 1 }
                }
            }
        ]
    ).exec().then((results) => {
        const x1 = results.map(function (item) {
            return item["_id"];
        });

        const y1 = results.map(function (item) {
            return item["count"];
        });

        res.send({
            MarsSol: x1,
            Photos: y1
        });
    });
});

app.get('/data/photosByDate', (req, res) => {
    RoverPhoto.aggregate(
        [
            {
                $group : {
                    _id: {
                        $dateToString: {
                            date: "$earth_date",
                            format: '%Y-%m-%d',
                        }},
                    count: { $sum: 1 }
                }
            }
        ]
    ).exec().then((results) => {
        const x1 = results.map(function (item) {
            return item["_id"];
        });

        const y1 = results.map(function (item) {
            return item["count"];
        });

        res.send({
            Date: x1,
            Photos: y1
        });
    });
});

app.listen(HTTP_PORT, () => console.log(`Solar-Backend listening on http://localhost:${HTTP_PORT}!`));

