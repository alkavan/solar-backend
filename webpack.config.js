const HtmlWebPackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const webpack = require("webpack");

module.exports = {
    context: __dirname,
    entry: "./src/app.js",

    output: {
        filename: "app.js",
        path: __dirname + "/dist",
    },

    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    query: {compact: false}
                }
            }
        ]
    },

    plugins: [
        new HtmlWebPackPlugin({
            template: "./public/index.html",
            filename: "index.html"
        }),
        new CopyPlugin([
            {
                from: 'public/css/',
                to: 'css/',
                toType: 'dir',
            },
            {
                from: 'public/favicon.ico',
                to: 'favicon.ico',
            },
        ])
    ],
    node: {
        fs: 'empty'
    }
};